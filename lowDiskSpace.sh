#!/bin/bash
 
URL="https://api.pushover.net/1/messages.json"
API_KEY=""
USER_KEY=""

TITLE="Low Disk Space" 

# Set this to be the minimum number of GB before we start notifying
notify=100
 
# Add as many paths to the df command to monitor as you want, or just use /
df /river /wash /kaylee --block-size=1GB | awk '{if (NR!=1) {print 
$6,"\t",$4 }}' | {
        while read x
        do
                location=`echo "$x" | awk '{print $1}'`
                remaining=`echo "$x" | awk '{print $2}'`
 
                if [[ $remaining -lt $notify ]]
                then
                        send=true
                        message="$message $location [$remaining GB] "
                fi
        done
 
        if [[ ! -z "$message" ]]
        then
              curl \
								-F "token=${API_KEY}" \
								-F "user=${USER_KEY}" \
								-F "title=${TITLE}" \
								-F "message=${message}" \
							"${URL}" > /dev/null 2>&1
        fi
}
